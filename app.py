from flask import Flask
from flask import render_template

import pandas as pd

app = Flask(__name__)

df = pd.DataFrame({'id':[1,2,3],'name':['qwe','qwer','sdf']})

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name,tables=[df.to_html()],titles=['id','name'])


if __name__ == "__main__":
    app.run(port=5000)
